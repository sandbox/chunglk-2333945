README.txt
------------
GDrive File is a file widget using FileField Sources module for upload 
files to Google Drive.

INSTALL 
------------ 
Install Droogle module first(https://www.drupal.org/project/Droogle).

DROOGLE CONFIG AND SETUP 
------------------------ 
Go to admin/settings/droogle and put in a client id and client secret 
for the google account you would like to use.  With this latest version
of the google drive api, which uses more secure OAUTH authentication, you'll
need to generate a client id and client secret for your google user account
at https://cloud.google.com/console.

1.)  Create a project
2.)  In the APIs tab (a link in the left sidebar) enable both the Drive API 
and the Drive SDK.
3.)  Generate a Client Id and Client Secret on the Credentials tab 
(its not really a tab its a link in the left sidebar).

You can optionally setup Organic Groups to look for a group cck field 
to read the google
client id and client secret for each group. 
 

The module is presented as is under the opensource license that
is within the module for you to read, we make no guarantees. That
being said we're looking to make this module great and feel free
to submit issues and suggestions to our issue que. 

GOOGLE API Exposed for Drupal Developers 
----------------------------------------- 
This version of Drupal has a function called droogle_file_upload 
and works great using the latest gdrive api as of 1/30/2014 to
create and share a gdrive document.

Other api functions previously in droogle, I'm hoping to port over
to this version (this version which uses the most up to date api).
